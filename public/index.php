<?php

use App\Form\Field\SelectField;
use App\Form\Form;
use App\Http\Request;
use App\Kernel;
use App\Model\PopulationRequest;
use App\Template;
use App\Cache\VerySimpleFileCache;
use App\Service\Population;
use App\Client\CurlClient;

require_once __DIR__ . "/../src/autoloader.php";

$app = new Kernel();

$app->get('/', function (Request $request = null) {
    $cache = new VerySimpleFileCache();

    if (!$cache->has('countries')) {
        $population = new Population(new CurlClient());
        $cache->set('countries', $population->getCountries());
    }
    $countries = $cache->get('countries');


    $form = new Form("population", "/");
    $form->addField(new SelectField("age", '', array_combine($r = range(0, 100), $r)))
        ->addField(new SelectField("year", '', array_combine($r = range(1950, date('Y')), $r)))
        ->addField(new SelectField("country", '', array_combine($countries, $countries)));

    $template = new Template('templates/index.phtml');
    return $template->render(['form' => $form]);
});

$app->post('/', function (Request $request = null) {
    $cache = new VerySimpleFileCache();

    if (!$cache->has('countries')) {
        $population = new Population(new CurlClient());
        $cache->set('countries', $population);
    }
    $countries = $cache->get('countries');

    $form = new Form("population", "/");

    $form->addField(new SelectField("age", '', array_combine($r = range(0, 100), $r)))
        ->addField(new SelectField("year", '', array_combine($r = range(1950, date('Y')), $r)))
        ->addField(new SelectField("country", '', array_combine($countries, $countries)));

    $form->addValidator(function (Form $form) {
        $count = 0;
        /** @var \App\Form\Field\FieldInterface $field */
        foreach ($form->getFields() as $field) {
            if ($field->getValue() !== null) {
                $count++;
            }
        }
        if ($count == 0) {
            $form->addError("Форма не должна быть пустой");
        }
    });
    $form->addValidator(function (Form $form) {
        $count = 0;
        /** @var \App\Form\Field\FieldInterface $field */
        foreach ($form->getFields() as $field) {
            if ($field->getValue() !== null) {
                $count++;
            }
        }
        if ($count == 1) {
            $form->addError("Должно быть заполнено больше одного поля");
        }
    });

    $form->handleRequest($request);
    $population = [];

    if ($form->isValid()) {
        $populationService = new Population(new CurlClient());
        $form->setModel(PopulationRequest::class);
        $data = $form->getData();
        $population = $populationService->getPopulation($data);
    }
    $template = new Template('templates/population.phtml');

    return $template->render(['form' => $form, 'population' => $population]);
});
$app->run();
