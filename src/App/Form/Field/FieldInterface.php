<?php

namespace App\Form\Field;

interface FieldInterface
{
    public function getValue();
    public function setValue(string $value);
    public function render(): string;
}
