<?php

namespace App\Form\Field;

/**
 * Class SelectField
 * @package App\Form\Field
 */
class SelectField implements FieldInterface
{
    private $name;
    private $value;
    private $data;
    private $required;

    /**
     * SelectField constructor.
     * @param string $name
     * @param string $value
     * @param array $data
     * @param bool $required
     */
    public function __construct(string $name='', string $value='', array $data = [], bool $required=false)
    {
        $this->name = $name;
        $this->value = $value;
        $this->required = $required;
        $this->data = $data;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): SelectField
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue(string $value): SelectField
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @param bool $required
     * @return $this
     */
    public function setRequired(bool $required): SelectField
    {
        $this->required = $required;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        if ($this->value !== '') {
            return $this->value;
        }
        return null;
    }

    public function getRequired(): bool
    {
        return $this->required;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data = []): SelectField
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        $res = "<select";
        $res .= " id='". $this->name ."' name='". $this->name ."'";
        if ($this->required) {
            $res .= " required='true'";
        }
        $res .= ">";
        $res .= "<option></option>";
        foreach ($this->data as $k => $v) {
            $res .= "<option value='".$k ."'";
            if ($this->value === (string)$k) {
                $res .= " selected";
            }
            $res .= ">". $v . "</option>";
        }
        $res .= "<select/>";

        return $res;
    }
}
