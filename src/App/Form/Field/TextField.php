<?php

namespace App\Form\Field;

/**
 * Class TextField
 * @package App\Form\Field
 */
class TextField implements FieldInterface
{
    private $name;
    private $value;
    private $required;

    /**
     * TextField constructor.
     * @param string $name
     * @param string $value
     * @param bool $required
     */
    public function __construct(string $name='', string $value='', bool $required=false)
    {
        $this->name = $name;
        $this->value = $value;
        $this->required = $required;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): TextField
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue(string $value): TextField
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @param bool $required
     * @return $this
     */
    public function setRequired(bool $required): TextField
    {
        $this->required = $required;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        if ($this->value !== '') {
            return $this->value;
        }
        return null;
    }

    /**
     * @return bool
     */
    public function getRequired(): bool
    {
        return $this->required;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        $res = "<input type='text'";
        $res .= " id='". $this->name ."' name='". $this->name ."'";

        if ($this->value != '') {
            $res .= " value='".$this->value."'";
        }

        if ($this->required) {
            $res .= " required='true'";
        }
        $res .= "/>";

        return $res;
    }
}
