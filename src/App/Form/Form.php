<?php

namespace App\Form;

use App\Form\Field\FieldInterface;
use App\Http\Request;

/**
 * Class Form
 * @package App\Form
 */
class Form
{
    private $fields;
    private $errors;
    private $method;
    private $name;
    private $action;

    private $model;

    private $validators;

    /**
     * Form constructor.
     * @param string $name
     * @param string $action
     * @param string $method
     */
    public function __construct($name="", $action = "", $method="POST")
    {
        $this->errors = [];
        $this->fields = [];
        $this->name = $name;
        $this->method = $method;
        $this->action = $action;

        $this->validators = [];
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        while (count($this->validators) != 0) {
            $validator = array_pop($this->validators);
            \call_user_func_array($validator, [$this]);
        }

        return \count($this->errors) <= 0;
    }

    /**
     * @param Request|null $request
     */
    public function handleRequest(?Request $request)
    {
        if ($request) {
            $data = $request->request;
            if ($this->method == "GET") {
                $data = $request->query;
            }
            foreach ($data as $k => $v) {
                if (array_key_exists($k, $this->fields)) {
                    $this->getField($k)->setValue($v);
                }
            }
        }
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param \Closure $validator
     * @return $this
     */
    public function addValidator(\Closure $validator)
    {
        $this->validators[] = $validator;

        return $this;
    }

    /**
     * @param FieldInterface $field
     * @return $this
     */
    public function addField(FieldInterface $field)
    {
        $this->fields[$field->getName()] = $field;

        return $this;
    }

    public function getField(string $name)
    {
        return $this->fields[$name];
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function addError(string $error)
    {
        $this->errors[] = $error;

        return $this;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function setModel(string $model)
    {
        $this->model = $model;
    }

    /**
     * @return array
     */
    public function getData()
    {
        if ($this->model !== null) {
            $ob = new $this->model();
            foreach ($this->fields as $field) {
                $method = 'set'.\ucfirst($field->getName());
                if (\method_exists($ob, $method)) {
                    $ob->$method($field->getValue());
                }
            }

            return $ob;
        }

        $res = [];
        foreach ($this->fields as $field) {
            $res[$field->getName()] = $field->getValue();
        }

        return $res;
    }
}
