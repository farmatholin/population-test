<?php
namespace App;

use App\Http\Request;

/**
 * Class Kernel
 * @package App
 */
class Kernel
{
    private $controllers;
    private $allowedMethods;

    /**
     * Kernel constructor.
     */
    public function __construct()
    {
        $this->controllers = [];
        $this->allowedMethods = [
            'GET',
            'POST',
        ];
    }

    /**
     * @param string $path
     * @param \Closure $func
     */
    public function get(string $path, \Closure $func): void
    {
        $this->controllers['GET'] = [
            $path => $func,
        ];
    }

    /**
     * @param string $path
     * @param \Closure $func
     */
    public function post(string $path, \Closure $func): void
    {
        $this->controllers['POST'] = [
            $path => $func,
        ];
    }

    /**
     * Run Application
     *
     * @return void
     */
    public function run(): void
    {
        $request = new Request();

        $request->init();
        $this->handle($request);
    }

    /**
     * Handle request
     *
     * @param Request $request
     * @return void
     */
    private function handle(Request $request): void
    {
        if (!\in_array($request->getMethod(), $this->allowedMethods)) {
            header("405 Method not allowed", true, 403);
            return;
        }

        $methods = $this->controllers[$request->getMethod()];
        if (!\array_key_exists($request->getUri(), $methods)) {
            header("404 Not Found", true, 404);
            return;
        }
        $method = $methods[$request->getUri()];

        if ($method === null || !\is_callable($method)) {
            header("403 Bad Request", true, 403);
            return;
        }

        $funcArgs = [];
        $funcArgs[] = $request;

        echo \call_user_func_array($method, $funcArgs);
    }
}
