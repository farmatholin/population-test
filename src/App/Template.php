<?php

namespace App;

/**
 * Class Template
 * @package App
 */
class Template
{
    private $path;

    /**
     * Template constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * @param array $parameters
     * @return false|string
     */
    public function render(array $parameters = [])
    {
        \ob_start();
        foreach ($parameters as $key => $value) {
            ${$key} = $value;
        }
        // Проверка на то что передаётся тут не производится
        //  По хорошему надо проверять что мы передаёт
        include(__DIR__ . '/../../' . $this->path);
        return \ob_get_clean();
    }
}
