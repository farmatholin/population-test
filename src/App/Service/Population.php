<?php

namespace App\Service;

use App\Client\ClientInterface;
use App\Model\PopulationRequest;

/**
 * Class Population
 * @package App\Service
 */
class Population
{
    private $client;

    /**
     * Population constructor.
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @return array
     */
    public function getCountries(): array
    {
        $response = $this->client->get('http://api.population.io:80/1.0/countries');
        if ($response->getStatusCode() === 200) {
            return json_decode($response->getBody(), true)['countries'];
        }

        return [];
    }

    /**
     * @param PopulationRequest $request
     * @return array
     */
    public function getPopulation(PopulationRequest $request): ?array
    {
        $url = '';
        if ($request->getAge() !== null && $request->getCountry() != null && $request->getYear() !== null) {
            $url = \sprintf(
                "http://api.population.io:80/1.0/population/%s/%s/%s/",
                $request->getYear(),
                $request->getCountry(),
                $request->getAge()
            );
        } elseif ($request->getAge() !== null && $request->getYear() !== null && $request->getCountry() === null) {
            $url = \sprintf(
                "http://api.population.io:80/1.0/population/%s/aged/%s/",
                $request->getYear(),
                $request->getAge()
            );
        } elseif ($request->getAge() === null && $request->getYear() !== null && $request->getCountry() !== null) {
            $url = \sprintf(
                "http://api.population.io:80/1.0/population/%s/%s/",
                $request->getYear(),
                $request->getCountry()
            );
        } elseif ($request->getAge() !== null && $request->getYear() === null && $request->getCountry() !== null) {
            $url = \sprintf(
                "http://api.population.io:80/1.0/population/%s/%s/",
                $request->getCountry(),
                $request->getAge()
            );
        }

        if ($url !== '') {
            $data = $this->client->get($url);
            if ($data->getStatusCode() == 200) {
                return json_decode($data->getBody(), true);
            }
        }

        return [];
    }
}
