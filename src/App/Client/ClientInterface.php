<?php

namespace App\Client;

/**
 * Interface ClientInterface
 * @package App\Client
 */
interface ClientInterface
{
    /**
     * @param string $url
     * @param $body
     * @return ClientResponse
     */
    public function post(string $url, array $body): ClientResponse;

    /**
     * @param string $url
     * @return ClientResponse
     */
    public function get(string $url): ClientResponse;
}
