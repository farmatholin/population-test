<?php

namespace App\Client;

/**
 * Class ClientResponse
 * @package App\Client
 */
class ClientResponse
{
    private $statusCode;
    private $headers;
    private $body;

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return ClientResponse
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param mixed $headers
     * @return ClientResponse
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     * @return ClientResponse
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }
}
