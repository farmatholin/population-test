<?php

namespace App\Client;

/**
 * Class CurlClient
 * @package App\Client
 */
class CurlClient implements ClientInterface
{
    /**
     * @param string $url
     * @param array $body
     * @return ClientResponse
     */
    public function post(string $url, array $body = []): ClientResponse
    {
        return $this->request($url, "POST", $body);
    }

    /**
     * @param string $url
     * @return ClientResponse
     */
    public function get(string $url): ClientResponse
    {
        return $this->request($url);
    }

    /**
     * @param string $url
     * @param string $method
     * @param array $body
     * @return ClientResponse
     */
    private function request($url = "", $method = "GET", $body = []): ClientResponse
    {
        $response = new ClientResponse();

        $curl = curl_init($url);

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, 1);
            $data = \http_build_query($body);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $responseBody = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        $response->setBody($responseBody);
        $response->setStatusCode((int)$info['http_code']);

        return $response;
    }
}
