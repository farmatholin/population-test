<?php

namespace App\Cache;

/**
 * Class VerySimpleFileCache
 */
class VerySimpleFileCache
{
    private $dir;

    /**
     * VerySimpleFileCache constructor.
     * @param string $dir
     */
    public function __construct(string $dir = __DIR__. '/../../../cache')
    {
        $this->dir = $dir;
    }

    /**
     * @param $key
     * @return bool
     */
    public function has($key)
    {
        $name = $this->dir .'/'. $key.'.cache';
        if (\file_exists($name)) {
            return true;
        }

        return false;
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    public function set($key, $value)
    {
        $name = $this->dir .'/'. $key.'.cache';
        $cached = json_encode($value);
        if (file_put_contents($name, $cached, LOCK_EX)) {
            return true;
        }

        return false;
    }

    /**
     * @param $key
     * @return bool|mixed
     */
    public function get($key)
    {
        if (!$this->has($key)) {
            return false;
        }

        $name = $this->dir .'/'. $key.'.cache';
        if ($data = file_get_contents($name)) {
            return \json_decode($data, true);
        }

        return false;
    }
}
