<?php

namespace App\Model;

/**
 * Class PopulationRequest
 * @package App\Model
 */
class PopulationRequest
{
    private $year;
    private $country;
    private $age;

    /**
     * @param $year
     * @return $this
     */
    public function setYear($year): PopulationRequest
    {
        $this->year = $year;

        return $this;
    }

    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param $country
     * @return PopulationRequest
     */
    public function setCountry($country): PopulationRequest
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param $age
     * @return $this
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }
}
