<?php

namespace App\Http;

/**
 * Class Request
 * @package App\Http
 */
class Request
{
    // Эти поля я сделал открытыми для простоты.
    public $server;
    public $query;
    public $request;

    private $method;
    private $uri;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->query = [];
        $this->server = [];
        $this->request = [];
    }

    /**
     * @return $this
     */
    public function init()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $requestUrl = $_SERVER['REQUEST_URI'];
        $this->uri = $requestUrl;

        if (($pos = strpos($requestUrl, '?')) !== false) {
            $this->uri = substr($requestUrl, 0, $pos);
        }

        foreach ($_SERVER as $key => $value) {
            $this->server[$this->toCamelCase($key)] = $value;
        }

        foreach ($_GET as $key => $value) {
            $this->query[$this->toCamelCase($key)] = $value;
        }

        if ($this->method === 'POST') {
            foreach ($_POST as $key => $value) {
                $this->request[$this->toCamelCase($key)] = $value;
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param $string
     * @return mixed|string
     */
    private function toCamelCase($string)
    {
        $result = \strtolower($string);
            
        \preg_match_all('/_[a-z]/', $result, $matches);
        foreach ($matches[0] as $match) {
            $c = \str_replace('_', '', \strtoupper($match));
            $result = \str_replace($match, $c, $result);
        }
        return $result;
    }
}
